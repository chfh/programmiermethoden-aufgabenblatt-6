/*
Copyright (c) 2012, 
Christian Holzberger <christian.holzberger@fh-bielefeld.de>
Christopher Reincke <christopher.reincke@fh-bielefeld.de>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL EITHER CHRISTIAN HOLZBERGER OR CHRISTOPHER REINCKE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package systemTests;

import Lexer.Exceptions.UnknownTokenException;
import Lexer.Lexer;
import Lexer.Token.*;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author cholzberger
 */
public class LexerTest {

	/**
	 * 
	 */
	public LexerTest() {
	}

	/**
	 * Test of parseString with ANNOTATION method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseAnnotation() throws Exception {
		String source = "noise @testme annotation";
		Lexer instance = new Lexer();
	
		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(AnnotationToken.class, t.getClass());
		assertEquals("@testme", t.getValue());
	}

	/**
	 * Test of parseString with SEMICOLON method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseSemicolon() throws Exception {
		String source = "noise ; annotation";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(SemicolonToken.class, t.getClass());
		assertEquals(";", t.getValue());
	}

	/**
	 * Test of parseString with WHITESPACE method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseWhitespace() throws Exception {
		String source = "noise   annotation";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(WhitespaceToken.class, t.getClass());
		assertEquals(" ", t.getValue());
	}

	/**
	 * Test of parseString with LEFTBRACE method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseBrace() throws Exception {
		String source = "noise { annotation";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(BraceToken.class, t.getClass());
		assertEquals("{", t.getValue());
	}

	
	/**
	 * Test of parseString with OPERATOR method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseOperator() throws Exception {
		String[] operators = {"=", "+", "-", "/", "&&", "||", "|", "&", "*"};

		for (String operator : operators) {
			String source = "noise "+operator+" annotation";
			Lexer instance = new Lexer();

			instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
			Token t = result.get(2);
			assertEquals(OperatorToken.class, t.getClass());
			assertEquals(operator, t.getValue());
		}
	}
	
	/**
	 * Test of parseString with JAVADOC method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseJavadoc() throws Exception {
		System.out.println("StartTest\n\n");
		String source = "noise /** docedidocedidoc...voll n */ annotation";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(CommentJavadocToken.class, t.getClass());
		assertEquals("/** docedidocedidoc...voll n */", t.getValue());
	}
	
	/**
	 * Test of parseString with JAVADOC method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseJavadocMultiline() throws Exception {
		System.out.println("StartTest Multiline\n\n");
		
		String source = "noise /**\n docedi\ndocedidoc...voll n \n*/ annotation";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(CommentJavadocToken.class, t.getClass());
		assertEquals("/**\n docedi\ndocedidoc...voll n \n*/", t.getValue());
	}
	
	/**
	 * Test of parseString with COMMENT_MULTILINE method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseCommentMultiline() throws Exception {
		System.out.println("StartTest\n\n");
		String source = "noise /* docedidocedidoc...voll n */ annotation";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(CommentMultilineToken.class, t.getClass());
		assertEquals("/* docedidocedidoc...voll n */", t.getValue());
	}
	
	/**
	 * Test of parseString with COMMENT_MULTILINE method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseCommentMultilineMultiline() throws Exception {
		System.out.println("StartTest Multiline\n\n");
		
		String source = "noise /*\n docedi\ndocedidoc...voll n \n*/ annotation";
		Lexer instance = new Lexer();

	instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(CommentMultilineToken.class, t.getClass());
		assertEquals("/*\n docedi\ndocedidoc...voll n \n*/", t.getValue());
	}
	
	/**
	 * Test of parseString with COMMENT_SINGLELINE method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseCommentSingleline() throws Exception {
		
		String source = "noise // docedidocedidoc a";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(CommentSinglelineToken.class, t.getClass());
		assertEquals("// docedidocedidoc a", t.getValue());
	}
	
	/**
	 * Test of parseString with IMPORT method, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testParseImport() throws Exception {
		
		String source = "noise import b";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
		Token t = result.get(2);
		assertEquals(ImportToken.class, t.getClass());
		assertEquals("import", t.getValue());
	}
	
	/**
	 * Test of parseString with INVALID data, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test (expected=UnknownTokenException.class)
	public void testInvalidParseString() throws Exception {
		System.out.println("testInvalidParse");
		String source = "\"asdasd";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
	}
	
	/**
	 * Test of parseString with INVALID data, of class Lexer.
	 * 
	 * @throws Exception 
	 */
	@Test (expected=UnknownTokenException.class)
	public void testInvalidParseChar() throws Exception {
		
		String source = "'asdasd'";
		Lexer instance = new Lexer();

		instance.setSource(source);
		List<Token> result = instance.parseSource( Token.WHITESPACE);
	}
}
