/*
Copyright (c) 2012, 
Christian Holzberger <christian.holzberger@fh-bielefeld.de>
Christopher Reincke <christopher.reincke@fh-bielefeld.de>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL EITHER CHRISTIAN HOLZBERGER OR CHRISTOPHER REINCKE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package Lexer;

import Lexer.Exceptions.UnknownTokenException;
import Logger.NoTimestampFormatter;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hauptprogramm fuer den Lexer
 * @author cholzberger
 */
public class LexerMain {
	/** 
	 * hauptprogramm
	 */
	public void run() {
		Lexer l = null;
		try {
			 l = new Lexer();
		} catch (IOException e) {
			System.err.println("Fataler Fehler: " + e.getLocalizedMessage());
			System.exit(1);
		} catch (Exception e ) {
			System.err.println("Unbekannter fataler Fehler: " + e.getLocalizedMessage());
			System.exit(1);
		}
		l.setLoggin(Boolean.TRUE, Boolean.TRUE);
		
		l.setSource("\timport int main();\n//asd");
		
		try {
			System.out.println(l.toText());
		} catch (UnknownTokenException e) {
			System.err.println("Fataler Fehler: " + e.getLocalizedMessage());
			System.exit(1);
		}  catch (Exception e ) {
			System.err.println("Unbekannter fataler Fehler: " + e.getLocalizedMessage());
			System.exit(1);
		}
	}
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		Logger systemLogger = Logger.getLogger("");
		systemLogger.setLevel(Level.ALL);
		
		/** ?! **/
		for ( Handler h : systemLogger.getHandlers() ) {
			h.setLevel(Level.OFF);
		}
	
		LexerMain l = new LexerMain();
		l.run();
	}
}
