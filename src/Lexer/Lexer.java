/*
Copyright (c) 2012, 
Christian Holzberger <christian.holzberger@fh-bielefeld.de>
Christopher Reincke <christopher.reincke@fh-bielefeld.de>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL EITHER CHRISTIAN HOLZBERGER OR CHRISTOPHER REINCKE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package Lexer;

import Lexer.Exceptions.UnknownTokenException;
import Lexer.Token.Token;
import Lexer.Token.TokenFactory;
import Logger.NoTimestampFormatter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * @author Chris
 * @author cholzberger
 */
public class Lexer {
	List<LexerObserver> observer;
	private Boolean textLogging = false;
	private Boolean htmlLogging = false;
	private Logger textLogger;
	private Logger htmlLogger;
	private String source;
	private List<Token> tokens;

	/**
	 * Initialisiert den neuen Lexer
	 *
	 * @throws IOException
	 */
	public Lexer() throws IOException {
		textLogger = Logger.getLogger(this.getClass().getName() + ".textLogger");
		htmlLogger = Logger.getLogger(this.getClass().getName() + ".htmlLogger");

		FileHandler htmlFileComments = new FileHandler("source-with-comments.html");
		FileHandler htmlFileNoComments = new FileHandler("source-without-comments.html");
		ConsoleHandler textHandler = new ConsoleHandler();

		textHandler.setFormatter(new NoTimestampFormatter());
		htmlFileComments.setFormatter(new NoTimestampFormatter());
		htmlFileNoComments.setFormatter(new NoTimestampFormatter());

		htmlFileComments.setLevel(Level.ALL);
		htmlFileNoComments.setLevel(Level.FINE);

		textLogger.setLevel(Level.INFO);


		// fixme: Formatter schreiben
		htmlLogger.addHandler(htmlFileComments);
		htmlLogger.addHandler(htmlFileNoComments);

		textLogger.addHandler(textHandler);
		
		observer = new ArrayList<>();
	}

	/**
	 * gibt den derzeitig betrachteten Quelltext zurueck
	 *
	 * @return source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * setzt den derzeigen Quelltext
	 *
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
		
		for ( LexerObserver o: observer) {
				o.onSourceUpdate();
			}
		
	}

	/**
	 * liest den source aus einer datei
	 * 
	 * @param file
	 * @throws FileNotFoundException
	 * @throws IOException  
	 */
	public void setSource(File file) throws FileNotFoundException, IOException {
		BufferedReader br = null;
		String ret = "";

		try {
			br = new BufferedReader(new FileReader(file));

			while (true) {
				String tmp = br.readLine();
				if (tmp == null) {
					break;
				}
				ret += tmp + "\n";
			}

			setSource(ret);
		} catch (Exception e) {
			setSource("");
			throw e;
		} finally {
			if (br != null) {
				try {	br.close(); } catch (IOException e ) {}
			}
		}
	}

	/**
	 * parst den Quelltest der ueber setSource gesetzt wurde speichert die Token in der Klasse zwischen UND gibt sie zurueck
	 *
	 * @param mode
	 * @return Liste der Tokens
	 * @throws UnknownTokenException
	 */
	public List<Token> parseSource(int mode) throws UnknownTokenException {
		if (source.length() == 0) {
			return new ArrayList<>();
		}
		
		if ( htmlLogging) {
			htmlLogger.info("<br/><br/><strong>Neuer Parse-Vorgang:</strong><br/>");
		}

		int start = 0;
		tokens = new ArrayList<>();
		while (start != source.length()) {
			start = TokenFactory.parse(source, start, tokens, mode);
		}

		return tokens;
	}

	/**
	 * gibt die token als Text formatiert zurueck
	 *
	 * @return
	 * @throws UnknownTokenException
	 */
	public String toText() throws UnknownTokenException {
		String text = "Running Lexer:\n";
		List<Token> list;
		list = parseSource(Token.NOWHITESPACE);

		for (Token t : list) {
			text += t + "\n";
		}

		return text;
	}

	/**
	 * gibt die Token als HTML formatiert zurueck
	 *
	 * @return
	 * @throws UnknownTokenException
	 */
	public String toHtml() throws UnknownTokenException {
		String htmlText = "<html><head></head><body>\n<pre>";
		List<Token> list;

		list = parseSource(Token.WHITESPACE);

		for (Token t : list) {
			htmlText += t.toHtml();
		}
		return htmlText;

	}

	/**
	 * logging ein / aus-schalten
	 *
	 * @param textLogging Als text auf Konsole loggen
	 * @param htmlLogging Als html in datei loggen
	 */
	public void setLoggin(Boolean textLogging, Boolean htmlLogging) {
		this.textLogging = textLogging;
		this.htmlLogging = htmlLogging;
		
		if (htmlLogging) {
			TokenFactory.setHtmlLogger(htmlLogger);
		} else {
			TokenFactory.setHtmlLogger(null);
		}

		if (textLogging) {
			TokenFactory.setTextLogger(textLogger);
		} else {
			TokenFactory.setTextLogger(null);
		}
	}
	
	/**
	 * registriert einen LexerObserver
	 * @param lo
	 */
	public void addObserver(LexerObserver lo) {
		observer.add(lo);
	}
}
