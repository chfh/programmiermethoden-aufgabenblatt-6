/*
Copyright (c) 2012, 
Christian Holzberger <christian.holzberger@fh-bielefeld.de>
Christopher Reincke <christopher.reincke@fh-bielefeld.de>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL EITHER CHRISTIAN HOLZBERGER OR CHRISTOPHER REINCKE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package Lexer.Token;

import Lexer.Exceptions.UnhandledTokenException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Stellt einen GenericToken dar
 *
 * @author Chriz
 */
public abstract class Token implements Cloneable {
	/**
	 * der parser soll keine whitespaces berücksichtigen
	 */
	public static int NOWHITESPACE=0;
	/**
	 * der parser soll die whitespaces berücksichtigen
	 */
	public static int WHITESPACE=1;
	
	private String name;
	private String value;
	private Pattern pattern;
	private int start;
	private int end;

	private Logger textLogger=null;
	private Logger htmlLogger=null;
	
	/**
	 * gibt den aktuellen html logger zurueck
	 * null wenn nicht gesetzt
	 * @return 
	 */
	public Logger getHtmlLogger() {
		return htmlLogger;
	}

	/** 
	 * setzt den aktuellen logger, null wenn nicht benoetigt
	 * @param htmlLogger 
	 */
	public void setHtmlLogger(Logger htmlLogger) {
		this.htmlLogger = htmlLogger;
	}

	/**
	 * gibt den aktuellen text logger zurueck
	 * null wenn nicht gesetzt
	 * @return 
	 */
	public  Logger getTextLogger() {
		return textLogger;
	}

	/**
	 * setzt den aktuellen logger zurueck
	 * null wenn nicht benoetigt
	 * @param textLogger 
	 */
	public  void setTextLogger(Logger textLogger) {
		this.textLogger = textLogger;
	}

	/**
	 * gibt den Modus des Token zurueck
	 * @return 
	 */	
	public int getMode() {
		return mode;
	}

	/**
	 * setzt den aktuellen Modus
	 * @param mode 
	 */
	public void setMode(int mode) {
		this.mode = mode;
	}
	
	int mode = NOWHITESPACE;
	
	/**
	 * erzeugt eine kopie des aktuellen Tokens
	 */
	@Override
	public Token clone() throws CloneNotSupportedException {
		return (Token) super.clone();
	}
	
	/** 
	 * gibt den typen des Tokens zurueck
	 * @return 
	 */
	public String getType() {
		return getClass().getSimpleName();
	}
	
	/** 
	 * gibt den Wert des Tokens zurueck
	 * @return 
	 */
	public String getValue() {
		return value;
	}

	/** 
	 * setzt den wert dieses Tokens
	 * @param value 
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/** 
	 * erzeugt einen neuen Token
	 * @param name
	 * @param pattern 
	 */
	public Token(String name, String pattern) {
		this.name = name;
		this.pattern = Pattern.compile(pattern, Pattern.DOTALL);
	}

	/** 
	 * gibt den Namen diese Tokens zurueck
	 * @return 
	 */
	public String getName() {
		return name;
	}

	/**
	 * gibt das pattern dieses Tokens zurueck
	 * @return Pattern
	 */
	public Pattern getPattern() {
		return this.pattern;
	}
	
	/**
	 * gibt diesen token als string aus
	 * @return 
	 */
	@Override
	public String toString() {
		return "Token{" + "type=" + getType() + ", value=" + value + ", start="+start+", end="+end+"}";
	}

	/**
	 * gibt diesen token als html aus
	 * @return 
	 */
	public String toHtml() {
		return "<span class='"+ getType() + "'>" + value.replaceAll("<","&lt;").replaceAll(">","&gt;") + "</span>";
	}
	/**
	 * erhaelt den komplletten quellcode und interepretiert ihn ab start weiter
	 * clont sich selbst und fuegt sich am ende des trees an
	 * 
	 * @param source Quellcode
	 * @param start Start ab Zeichen
	 * @param tree Lexer-Tree
	 * @return ende des derzeigen Tokens
	 * @throws UnhandledTokenException 
	 */
	public int parse(String source, int start, List<Token>tree) throws UnhandledTokenException {
		Matcher m = pattern.matcher( source.substring(start) );
		Token ret;
		
		if (m.lookingAt()) {
			try {
				String text = m.group();
				ret = (Token) this.clone();
				ret.setStart(start);
				
				ret.setEnd(start + text.length());
				ret.setValue(text);
				
				beforeInsert(ret, tree,source);
				onInsert(ret,tree,source);
				afterInsert(ret, tree,source);
				return ret.getEnd();
			} catch (CloneNotSupportedException e) {
				System.out.println(e.getMessage());
			}

		}
		throw new UnhandledTokenException("Token not handled here");
	}

	/**
	 * gibt das ende dieses Tokens aus
	 * @return 
	 */
	public int getEnd() {
		return end;
	}

	/**
	 * setzt das Ende dieses tokens im source
	 * @param end 
	 */
	public void setEnd(int end) {
		this.end = end;
	}

	/**
	 * gibt den start dieses Tokens zurueck
	 * @return 
	 */
	public int getStart() {
		return start;
	}

	/**
	 * setzt den Start dieses Tokens
	 * @param start 
	 */
	public void setStart(int start) {
		this.start = start;
	}
	
	/**
	 * wird aufgerufen bevor der Token t der Liste l hinzugefuegt wird
	 * @param t
	 * @param l
	 * @param source 
	 */
	public void beforeInsert(Token t, List<Token> l, String source) {
	
	}
	
	/**
	 * wird aufgerufen nachdem der Token t der Liste l hinzugefuegt wurde
	 * @param t
	 * @param l
	 * @param source 
	 */
	public void afterInsert(Token t, List<Token> l, String source) {
		if ( htmlLogger != null ) {
			if ( t.getClass() == CommentJavadocToken.class || t.getClass() == CommentMultilineToken.class || t.getClass() == CommentSinglelineToken.class) {
				htmlLogger.finest(t.toHtml());
			} else {
				htmlLogger.fine(t.toHtml());
			}
		}
		
		if ( textLogger != null ) {
			textLogger.info(t.toString());
		}
	}
	
	/**
	 * Fuegt den Token t in die Liste l ein
	 * @param t
	 * @param l
	 * @param source 
	 */
	public void onInsert(Token t, List<Token> l, String source) {
		if ( mode == NOWHITESPACE ) {
			if ( l.size() > 0 && l.get(l.size()-1).getType().equals("WhitespaceToken")) {
				l.set(l.size()-1,t);
			} else {
				l.add(t);
			}
		} else {
			l.add(t);
		}
	}
}
