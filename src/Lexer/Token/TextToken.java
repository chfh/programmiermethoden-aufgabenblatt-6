/*
Copyright (c) 2012, 
Christian Holzberger <christian.holzberger@fh-bielefeld.de>
Christopher Reincke <christopher.reincke@fh-bielefeld.de>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL EITHER CHRISTIAN HOLZBERGER OR CHRISTOPHER REINCKE BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package Lexer.Token;

import java.util.List;

/**
 * Konkreter Token fuer Text
 * 
 * fuegt zwei gleiche aufeinanderfolgende TextTokens zusammen
 * wenn ein Whitespace dazwischen liegt.
 * 
 * @author cholzberger
 */
public class TextToken extends Token {

	
	/**
	 * Konstruktor
	 */
	public TextToken() {
		super("Text","(\\w|\\.)*");
	}
	/**
	 * fuegt Tokens zusammen
	 * @see TextToken
	 * 
	 * @param t jeweilige Token
	 * @param l Liste der Tokens
	 * @param source quelle
	 */
	@Override
	public void beforeInsert(Token t, List<Token> l, String source) {
		
		if ( l.size() > 2 && l.get(l.size()-1).getClass() == WhitespaceToken.class && l.get(l.size()-2).getClass() == TextToken.class) {
			t.setStart(l.get(l.size()-2).getStart());
			t.setValue ( l.get(l.size()-2).getValue() + l.get(l.size()-1).getValue() + t.getValue());
			l.remove(l.size()-1);
			l.remove(l.size()-1);
		}
	}
	
}
